const std = @import("std");
const warn = @import("std").debug.warn;
const c = @cImport({
    @cInclude("kero_platform.h");
});

pub fn main() void {
    var platform: c.kero_platform_t = std.mem.zeroes(c.kero_platform_t);
    c.KP_Init(&platform, 1280, 720, "Zig test");
    c.KP_Sleep(2000000000);
}
