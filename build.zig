const std = @import("std");
const path = std.os.path;
const Builder = std.build.Builder;

pub fn build(b: *Builder) void {
    const exe = b.addExecutable("hello", "hello.zig");
    exe.setBuildMode(b.standardReleaseOptions());
    exe.linkSystemLibrary("c");
    exe.linkSystemLibrary("X11");
    exe.linkSystemLibrary("m");
    exe.addIncludeDir("/home/nick/git/croaking-kero-c-libraries/include");

    b.default_step.dependOn(&exe.step);
}
